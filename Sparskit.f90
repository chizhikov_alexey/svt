program Sparskit2
  implicit none
  character(len = 5) :: name
  integer :: nmax, nzmax, n, nnz, ierr, im0, maxits, iout, lfil, iwk, i
  integer :: ia, ja, jlu, ju, levs, jw
  real*8 :: a, vv, rhs, sol, alu, eps, t1, t2, t3, t4, w
  allocatable a(:), ia(:), ja(:), jlu(:), ju(:), vv(:,:), rhs(:), sol(:), alu(:), w(:), levs(:), jw(:)
  print*, "Vvedite nazvanie faila:"
  read*, name
  print*, "Vvedite parametr pereobuslavlivatelia:"
  read*, lfil
  open(101, file = name)
  read(101, *) nmax
  allocate(ia(nmax+1))
  read(101, *) ia
  nzmax = ia(nmax + 1) - ia(1)
  allocate(a(nzmax), ja(nzmax))
  read(101, *) ja
  read(101, *) a
  close(101)
  im0 = 10
  eps  = 1.0D-08
  maxits = 100
  iout = 1
  !call readsk(nmax,nzmax,n,nnz,ia,ja,a,name,ierr)
  n = nmax
  iwk = n
  allocate(w(n), jw(3*n), levs(iwk), alu(n), jlu(n), ju(n), vv(n, im0 + 1), rhs(n), sol(n))
  Do i=1,n
    rhs(i) = sin(i*1.0)
  end do
  sol = 0d0
  call cpu_time(t1)
  call iluk(n,a,ja,ia,lfil,alu,jlu,ju,levs,iwk,w,jw,ierr)
  call cpu_time(t2)
  t2=t2-t1
  print*, 'Vremia inicializacii:', t2, 'sec.'
  call cpu_time(t3)
  call pgmres(n, im0, rhs, sol, vv, eps, maxits, iout, a, ja, ia, alu, jlu, ju, ierr)
  call cpu_time(t4)
  t4=t4-t3
  print*, 'Vremia reshenia:', t4, 'sec.'
  deallocate(a)
  deallocate(ia)
  deallocate(ja)
  deallocate(jlu)
  deallocate(ju)
  deallocate(vv)
  deallocate(rhs)
  deallocate(sol)
  deallocate(alu)
  deallocate(w)
  deallocate(levs)
  deallocate(jw)
end
