program Puasson2
  implicit none

  integer, parameter :: nvmax = 150000, nbmax = 1000000, ntmax = 2 * nvmax, namax = 10000000
  integer, parameter :: MaxWr = 10000000, MaxWi = 50000000
  integer :: nv, nvfix, labelV, fixedV
  real*8 :: vrt
  integer :: nb, nbfix, bnd, labelB, fixedB
  integer :: nc, labelC
  real*8 :: crv
  external ANI_CrvFunction
  integer :: nt, ntfix, tri, labelT, fixedT
  integer :: control
  real*8 :: Quality
  integer :: MetricFunction1, MetricFunction2
  external MetricFunction1, MetricFunction2
  real*8 :: rW
  integer :: iW
  integer :: nEStar, iERR
  real*8 :: t1, t2, t3, t4, t5, t6
  integer :: ia, ja, nRow, nCol
  real*8 :: a, b
  integer :: iDATAFEM, controlFEM
  real*8 :: dDATAFEM
  external FEM2Dext
  integer :: i, lfil
  real*8 :: alu, w
  integer :: jlu, ju, iwk, levs, jw
  integer :: nnz, ipar
  real*8 :: sol, fpar
  real*8 :: norm = 0, rhs

  include 'fem2Dtri.fd'
  include 'assemble.fd'

  allocatable labelV(:), fixedV(:), vrt(:,:), bnd(:,:), labelB(:), fixedB(:), &
              labelC(:), crv(:,:), tri(:,:), labelT(:), fixedT(:), control(:), &
              rW(:), iW(:), controlFEM(:), iDATAFEM(:), dDATAFEM(:), ia(:), ja(:), &
              a(:), b(:), alu(:), jlu(:), ju(:), w(:), levs(:), jw(:), sol(:), &
              fpar(:), ipar(:), rhs(:)

  allocate(labelV(nvmax), fixedV(nvmax), vrt(2, nvmax), bnd(2, nbmax), labelB(nbmax), &
           fixedB(nbmax), labelC(nbmax), crv(2, nbmax), tri(3, ntmax), labelT(ntmax), &
           fixedT(ntmax), control(6), rW(MaxWr), iW(MaxWi), iDATAFEM(1), controlFEM(3), &
           dDATAFEM(1), ia(nvmax), ja(namax), a(namax), b(nvmax), ipar(16), fpar(16))

  iERR = 0

  print*
  print*, 'lfil = '
  read*, lfil
  print*

  call cpu_time(t1)
  !
  call loadMani(nv, nvfix, nvmax, vrt, labelV, fixedV, &
                nb, nbfix, nbmax, bnd, labelB, fixedB, &
                nC, crv, labelC, nt, ntfix, ntmax, tri, &
                labelT, fixedT, "./simple2.ani")

  print*

  call graph_demo(nv, vrt, nt, tri, 'mesh_initial.ps', ' ')

  control(1) = 100
  control(2) = 100000000
  control(3) = 1
  control(4) = 1
  control(5) = 1
  control(6) = 0

  Quality = 0.4D0
  nEStar  = 500

  call mbaAnalytic(nv, nvfix, nvmax, vrt, labelV, fixedV, &
                   nb, nbfix, nbmax, bnd, labelB, fixedB, &
                   nc, crv, labelC, ANI_CrvFunction, nt, ntfix, &
                   ntmax, tri, labelT, fixedT, nEStar, Quality, &
                   control, MetricFunction1, MaxWr, MaxWi, rW, iW, iERR)

  print*

  if (iERR .ne. 0) then
    print*, "Ошибка в первой mbaAnalytic"
  end if

  Quality = 0.0010D0
  nEStar  = 200000

  call mbaAnalytic(nv, nvfix, nvmax, vrt, labelV, fixedV, &
                   nb, nbfix, nbmax, bnd, labelB, fixedB, &
                   nc, crv, labelC, ANI_CrvFunction, nt, ntfix, &
                   ntmax, tri, labelT, fixedT, nEStar, Quality, &
                   control, MetricFunction2, MaxWr, MaxWi, rW, iW, iERR)

  if (iERR .ne. 0) then
    print*, "Ошибка во второй mbaAnalytic"
  end if

  call graph_demo(nv,vrt, nt,tri, 'mesh_final.ps', ' ')

  call cpu_time(t2)

  t2=t2-t1
  print*, 'Построение сетки:', t2
  print*

  print*, 'Число узлов:', nv
  print*

  call cpu_time(t3)

  dDATAFEM(1) = 0D0
  iDATAFEM(1) = 0

  do i = 1, nv
    if(vrt(1,i) == 0d0) then
      labelV(i) = 1
    else if(vrt(2,i) == 1d0) then
      labelV(i) = 2
    else if(vrt(1,i) == 1d0) then
      labelV(i) = 3
    else if(vrt(2,i) == 0d0) then
      labelV(i) = 4
    else
      labelV(i) = 0
    end if
  end do

  controlFEM(1) = IOR(MATRIX_GENERAL, FORMAT_CSR)
  controlFEM(2) = 1

  call BilinearFormTemplate(nv, nb, nt, vrt, labelV, bnd, labelB, tri, labelT, &
                            FEM2Dext, dDATAFEM, iDATAFEM, controlFEM, &
                            nvmax, namax, ia, ja, a, b, nRow, nCol, &
                            MaxWi, MaxWr, iW, rW)

  print*

  call cpu_time(t4)

  t4=t4-t3
  print*, 'Построение системы: ', t4
  print*

  iERR = 0

  nnz = ia(nRow + 1) - ia(1)

  iwk = 15 * nnz + nRow

  allocate(w(5 * nRow), jw(3 * nRow), levs(iwk), alu(iwk), jlu(iwk), ju(iwk), sol(nRow), rhs(nRow))

  do i = 1, nRow
    rhs(i) = (dabs(vrt(1,i) - vrt(2,i)) + 0.00001)**1.5
  end do

  do i = 1, nRow
    sol(i) = 0d0
  end do

  call cpu_time(t5)

  call iluk(nRow,a,ja,ia,lfil,alu,jlu,ju,levs,iwk,w,jw,iERR)

  if (iERR .ne. 0) then
    print*, "Error"
  end if
  print*

  ipar(1) = 0
  ipar(2) = 2
  ipar(3) = 1
  ipar(4) = 5 * nRow
  ipar(5) = 10
  ipar(6) = 2000
  fpar(1) = 1.0D-8
  fpar(2) = 1.0D-10

10  call cg(nRow, b, sol, ipar, fpar, w)

  if (ipar(1).eq.1) then
    call amux(nRow, w(ipar(8)), w(ipar(9)), a, ja, ia)
    goto 10
  else if (ipar(1).eq.2) then
    call atmux(nRow, w(ipar(8)), w(ipar(9)), a, ja, ia)
    goto 10
  else if (ipar(1).eq.3 .or. ipar(1).eq.5) then
    call lusol(nRow, w(ipar(8)), w(ipar(9)), alu, jlu, ju)
    goto 10
  else if (ipar(1).eq.4 .or. ipar(1).eq.6) then
    call lutsol(nRow, w(ipar(8)), w(ipar(9)), alu, jlu, ju)
    goto 10
  else if (ipar(1).le.0) then
    if (ipar(1).eq.0) then
      print *, 'Successfully.'
    else if (ipar(1).eq.-1) then
      print *, 'Iterative solver has iterated too many times.'
    else if (ipar(1).eq.-2) then
      print *, 'Iterative solver was not given enough work space.'
      print *, 'The work space should at least have ', ipar(4), ' elements.'
    else if (ipar(1).eq.-3) then
      print *, 'Iterative solver is facing a break-down.'
    else
      print *, 'Iterative solver terminated. code =', ipar(1)
    endif
  endif

  print*

  call cpu_time(t6)

  t6=t6-t5
  print*, 'Решение системы: ', t6
  print*

  print*, 'Итераций: ', ipar(7)
  print*

  do i = 1, nRow
    sol(i) = sol(i) - rhs(i)
  end do

  do i = 1, nRow
    if(dabs(sol(i)) >= norm) then
      norm = dabs(sol(i))
    end if
  end do

  print*, 'Ошибка решения:', norm
  print*

end

integer function MetricFunction2(x, y, Metric)
  implicit none

  real*8 :: x, y, Metric(2, 2), x0 = 0.0, y0 = 0.0, x1 = 1.0, y1 = 1.0, &
            distance, p0p1x, p0p1y

  p0p1x = x1 - x0
  p0p1y = y1 - y0

  distance = abs(p0p1y * x - p0p1x * y + x1 * y0 - y1 * x0)/sqrt(p0p1y**2 + p0p1x**2)

  if(distance < 0.05) then
    Metric(1,1) = 1D0
    Metric(2,2) = 1D0
  else
    Metric(1,1) = 0D0
    Metric(2,2) = 0D0
  end if

  Metric(1,2) = 0D0
  Metric(2,1) = 0D0

  MetricFunction2 = 0

  return
end

integer function MetricFunction1(x, y, Metric)
  implicit none

  real*8 :: x, y, Metric(2, 2)

  Metric(1,1) = 1D0
  Metric(2,2) = 1D0
  Metric(1,2) = 0D0
  Metric(2,1) = 0D0

  MetricFunction1 = 0

  return
end

subroutine FEM2Dext(XY1, XY2, XY3, &
                    lbT, lbB, lbV, dDATA, iDATA, iSYS, &
                    LDA, A, F, nRow, nCol, &
                    templateR, templateC)
  implicit none

  include 'fem2Dtri.fd'
  include 'assemble.fd'

  real*8   XY1(*), XY2(*), XY3(*)
  integer  lbT, lbB(3), lbV(3)
  real*8   dDATA(*)
  integer  iDATA(*), iSYS(*), LDA
  real*8   A(LDA, *), F(*)
  integer  templateR(*), templateC(*)
  integer  Ddiff, Drhs, Dbc
  external Ddiff, Drhs, Dbc
  integer  i, j, k, ir, ic, label, ibc, nRow, nCol
  real*8   XYP(2, 3)
  real*8   x, y, eBC(1)
  logical  ifXbc
  real*8   hmin, hmin1, hmin2, hmin3
  COMMON   /cml/hmin

  nRow = 3
  nCol = 3

  do i = 1, 3
    templateR(i) = Vdof
    templateC(i) = Vdof
  end do

  label = lbT

  if((XY1(1) .eq. XY1(2)) .OR. (XY2(1) .eq. XY2(2)) .OR. (XY3(1) .eq. XY3(2))) then

    hmin1 = dsqrt((XY1(1) - XY2(1))**2 + (XY1(2) - XY2(2))**2)
    hmin2 = dsqrt((XY1(1) - XY3(1))**2 + (XY1(2) - XY3(2))**2)
    hmin3 = dsqrt((XY2(1) - XY3(1))**2 + (XY2(2) - XY3(2))**2)

    if((hmin1 <= hmin2) .AND. (hmin1 <= hmin3)) then
      hmin = hmin1
    else if((hmin2 <= hmin1) .AND. (hmin2 <= hmin3)) then
      hmin = hmin2
    else if((hmin3 <= hmin1) .AND. (hmin3 <= hmin2)) then
      hmin = hmin3
    end if
  end if

  call fem2Dtri(XY1, XY2, XY3, &
                GRAD, FEM_P1, GRAD, FEM_P1, &
                label, Ddiff, dDATA, iDATA, iSYS, 2, &
                LDA, A, ir, ic)

  call fem2Dtri(XY1, XY2, XY3, &
                IDEN, FEM_P0, IDEN, FEM_P1, &
                label, Drhs, dDATA, iDATA, iSYS, 4, &
                1, F, ir, ic)

  do i = 1, 2
    XYP(i, 1) = XY1(i)
    XYP(i, 2) = XY2(i)
    XYP(i, 3) = XY3(i)
  end do

  do k = 1, 3
    if(lbV(k) .NE. 0) then
      x = XYP(1, k)
      y = XYP(2, k)

      label = lbV(k)
      ibc = Dbc(x, y, label, dDATA, iDATA, iSYS, eBC)

      if(ifXbc(ibc, BC_DIRICHLET)) then
        call applyDIR(LDA, nRow, A, F, k, eBC(1))
      end if
    end if
  end do

  return
end

integer function Ddiff(x, y, label, dDATA, iDATA, iSYS, Coef)
  implicit none

  include 'fem2Dtri.fd'

  real*8  dDATA(*), x, y, Coef(MaxTensorSize, *)
  integer iDATA(*), label, iSYS(*)

  iSYS(1) = 1
  iSYS(2) = 1

  Coef(1, 1) = 1D0
  Ddiff = TENSOR_SCALAR

  return
end

integer function Drhs(x, y, label, dDATA, iDATA, iSYS, Coef)
  implicit none

  include 'fem2Dtri.fd'

  real*8  dDATA(*), x, y, Coef(MaxTensorSize, *)
  integer iDATA(*), label, iSYS(*)
  real*8  hmin
  real*8 :: x0 = 0.0, y0 = 0.0, x1 = 1.0, y1 = 1.0, distance, p0p1x, p0p1y, param

  COMMON /cml/hmin

  param = 12D0

  p0p1x = x1 - x0
  p0p1y = y1 - y0

  distance = dabs(p0p1y * x - p0p1x * y + x1 * y0 - y1 * x0)/dsqrt(p0p1y**2 + p0p1x**2)

  iSYS(1) = 1
  iSYS(2) = 1

  if(distance .eq. 0D0) then
    Coef(1, 1) = -6 * dsqrt(dabs(x - y) + 0.00001) * (dsqrt(2.0D0)/hmin) - 1.5/(dsqrt(dabs(x - y) + 0.00001))
  else if((distance < (hmin/dsqrt(2.0D0))) .AND. (distance > 0D0)) then
    Coef(1, 1) = -6 * dsqrt(dabs(x - y) + 0.00001) * (dsqrt(2.0D0)/hmin * dabs(x - y) * param) - 1.5/(dsqrt(dabs(x - y) + 0.00001))
  else if(distance >= (hmin/dsqrt(2.0D0))) then
    Coef(1, 1) = -1.5/(dsqrt(dabs(x - y) + 0.00001))
  end if

  Drhs = TENSOR_SCALAR

  return
end

integer function Dbc(x, y, label, dDATA, iDATA, iSYS, Coef)
  implicit none

  include 'fem2Dtri.fd'

  real*8  dDATA(*), x, y, Coef(MaxTensorSize, *)
  integer iDATA(*), label, iSYS(*)

  iSYS(1) = 1
  iSYS(2) = 1

  if(label .eq. 1) then
    Coef(1, 1) = (dabs(y) + 0.00001)**1.5
    Dbc = BC_DIRICHLET
  else if(label .eq. 2) then
    Coef(1, 1) = (dabs(x - 1) + 0.00001)**1.5
    Dbc = BC_DIRICHLET
  else if(label .eq. 3) then
    Coef(1, 1) = (dabs(y - 1) + 0.00001)**1.5
    Dbc = BC_DIRICHLET
  else if(label .eq. 4) then
    Coef(1, 1) = (dabs(x) + 0.00001)**1.5
    Dbc = BC_DIRICHLET
  end if

  return
end

function distdot(n,x,ix,y,iy)
  integer n, ix, iy
  real*8 distdot, x(*), y(*), ddot

  external ddot

  distdot = ddot(n,x,ix,y,iy)

  return
end
