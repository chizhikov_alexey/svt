#include <stdio.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>
#include "umfpack.h"

#define IDX(i, j) (size * (i) + (j))

double func(double x, double y)
{
    return pow((x - y) * (x - y) + 10e-8, 0.75);
}

double f(double x, double y)
{
    double temp = (x - y) * (x - y) + 10e-8;
    double temp1 = 3 / pow(temp, 0.25);
    double temp2 = 1.5 * (x - y) * (x - y) / pow(temp, 1.25);
    temp = temp1 - temp2;
    return temp;
}

int main()
{
    const int size = 1500;
    const int nonzero = (4 * (size - 1) + 5 * (size - 2) * (size - 2));
    const double h = 1.0 / (size - 1);

    int* row_index = malloc((size * size + 1) * sizeof(int));
    int* col_index = malloc(nonzero * sizeof(int));
    double* values = malloc(nonzero * sizeof(double));
    double* b = malloc(size * size * sizeof(double));
    double* true_x = malloc(size * size * sizeof(double));
    double* x = malloc(size * size * sizeof(double));

    row_index[size * size] = nonzero;
    int temp = 0;
    // 1-й цикл краевые условия
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            if (i == 0 || i == size - 1 || j == 0 || j == size - 1) {
                row_index[IDX(i, j)] = temp;
                values[temp] = 1;
                col_index[temp] = i * size + j;
                b[IDX(i, j)] = func(i * h, j * h);
                temp++;
            }
            else {
                row_index[IDX(i, j)] = temp;
                temp += 5;
            }

            true_x[IDX(i, j)] = func(i * h, j * h);
        }
    }

    //заполнение внутр значений
    //из методички
    //центр аппроксимация 2-й производной
    for (int i = 2; i < size; i++) {
        for (int j = 2; j < size; j++) {
            int start_poz = row_index[IDX(i - 1, j - 1)];

            values[start_poz] = 1.0 / (h * h);
            col_index[start_poz] = size * (i - 2) + j - 1;

            values[start_poz + 1] = 1.0 / (h * h);
            col_index[start_poz + 1] = size * (i - 1) + j - 2;

            values[start_poz + 2] = -4.0 / (h * h);
            col_index[start_poz + 2] = size * (i - 1) + j - 1;

            values[start_poz + 3] = 1.0 / (h * h);
            col_index[start_poz + 3] = size * (i - 1) + j;

            values[start_poz + 4] = 1.0 / (h * h);
            col_index[start_poz + 4] = size * i + j - 1;

            b[IDX(i - 1, j - 1)] = f(i * h, j * h);
        }
    }

    //старые функции

    double inform_init[UMFPACK_INFO], inform_solve[UMFPACK_INFO];
    double* null = (double*)NULL;
    int i;
    void *Symbolic, *Numeric;
    (void)umfpack_di_symbolic(size * size, size * size, row_index, col_index, values, &Symbolic, null, null);
    (void)umfpack_di_numeric(row_index, col_index, values, Symbolic, &Numeric, null, inform_init);
    printf("Размер сетки: %d\nВремя инициализации: %lf\n", size, inform_init[UMFPACK_NUMERIC_TIME] + inform_init[UMFPACK_SYMBOLIC_TIME]);
    umfpack_di_free_symbolic(&Symbolic);
    (void)umfpack_di_solve(UMFPACK_Aat, row_index, col_index, values, x, b, Numeric, null, inform_solve);
    umfpack_di_free_numeric(&Numeric);
    printf("Время решения: %f секунд\n", inform_solve[UMFPACK_SOLVE_TIME]);

    double max_diff = 0;
    for (int i = 0; i < size * size; i++) {
        double diff = fabs(x[i] - true_x[i]);
        if (diff > max_diff) {
            max_diff = diff;
        }
    }
    printf("Максимальная ошибка:  %lf \n", max_diff);

    free(row_index);
    free(col_index);
    free(values);
    free(b);
    free(x);
    free(true_x);
    return 0;
}