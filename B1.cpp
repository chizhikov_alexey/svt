#include <iostream>
#include <cmath>
#include <time.h>
#include <stdlib.h>
#include "umfpack.h"


#define n 1000
#define nonzero (4*(n-1) + 5*(n-2)*(n-2))
#define N n*n

double func(double x, double y) {
    return pow((x-y) * (x-y) + 10e-8, 0.75);
}

//laplas
double f(double x, double y) {
    double temp = (x-y) * (x-y) + 10e-8;
    double temp1 = 3 / pow(temp, 0.25);
    double temp2= 1.5 * (x-y) * (x-y) / pow(temp, 1.25);
    temp = temp1 - temp2;
    return temp;
}

const double h = 1.0 / (n - 1);

int min() {
    int *row_index = (int *) malloc(sizeof(int) * (N+1));
    int *col_index = (int *) malloc(sizeof(int) * nonzero);
    double *values = (double *) malloc(sizeof(double) * nonzero);
    double *b = (double *) malloc(sizeof(double) * N);
    double *true_x = (double *) malloc(sizeof(double) * N);
    double *x = (double *) malloc(sizeof(double) * N);

    row_index[N] = nonzero;
    int temp = 0;
    // 1-й цикл краевый условия
    for(int i = 0; i < n; i++) {
        for(int j = 0; j < n; j++) {
            int ind = i*n+j;
            if(i == 0 || i == n-1 || j == 0 || j == n-1) {
                row_index[ind] = temp;
                values[temp] = 1;
                col_index[temp] = i*n + j;
                b[ind] = func(i*h, j*h);
                temp++;
            } else {
                row_index[ind] = temp;
                temp += 5;
            }

            true_x[ind] = func(i*h, j*h);

        }
    }
    //printf("temp:  %d, nonzero: %d\n", temp, nonzero);
    std::cout << "temp: " << temp << " nonzero: " << nonzero << std::endl;

    //заполнение внутр значений
    //из методички
    //центр аппроксимация 2-й производной
    for(int i = 2; i < n; i++ ) {
        for(int j = 2; j < n; j++) {
            int ind = n*(i-1)+j-1;
            int start_poz = row_index[ind];

            values[start_poz] = 1.0/(h*h);
            col_index[start_poz] = n*(i-2)+j-1;

            values[start_poz+1] = 1.0/(h*h);
            col_index[start_poz+1] = n*(i-1)+j - 2;

            values[start_poz+2] = -4.0 / (h*h);
            col_index[start_poz+2] = n*(i-1)+j-1;

            values[start_poz+3] = 1.0/(h*h);
            col_index[start_poz+3] = n*(i-1)+j;

            values[start_poz+4] = 1.0/(h*h);
            col_index[start_poz+4] = n*i + j-1;

            b[ind] = f(i*h, j*h);


        }
    }

    //старые функции

    

    double Inform[UMFPACK_INFO], Inform2[UMFPACK_INFO]; 
    double *null = (double *) NULL ;
    int i ;
    void *Symbolic, *Numeric ;
    //транспонированная 
    (void) umfpack_di_symbolic (N, N, row_index, col_index, values, &Symbolic, null, null) ;
    (void) umfpack_di_numeric (row_index, col_index, values, Symbolic, &Numeric, null, Inform) ;
    //printf("n: %d\nInit time: %lf\n", n, Inform[UMFPACK_NUMERIC_TIME]);
    std::cout << "n = " << n << std::endl;
    std::cout << "Initialization: " << Inform[UMFPACK_NUMERIC_TIME] << std::endl;

    umfpack_di_free_symbolic (&Symbolic) ;
    (void) umfpack_di_solve (UMFPACK_Aat, row_index, col_index, values, x, b, Numeric, null, Inform2) ;
    umfpack_di_free_numeric (&Numeric) ;
    //printf("Solve time: %f \n", Inform2[UMFPACK_SOLVE_TIME]);
    std::cout << "Solving: " << Inform2[UMFPACK_SOLVE_TIME] << std::endl;
   
    double norm_max = 0;
    for(int i =  0; i < N; i++) {
        double diff = fabs(x[i] - true_x[i]);
	if(diff > norm_max) norm_max = diff;

    }
    //printf("error norm:  %lf \n", norm_max);
    std::cout << "Diff: " << norm_max << std::endl;

    free(row_index);
    free(col_index);
    free(values);
    free(b);
    free(x);
    free(true_x);
    return 0;
}