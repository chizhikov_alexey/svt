#include <mpi.h>

#include <mkl.h>

#include <cmath>
#include <cstdio>
#include <cstdlib>

#define world MPI_COMM_WORLD

#define matrix_size 8192

double MatrixFormula(int i, int j)
{
	//if (i == j) return 1.0; else return 0.0;
	return -1.0 / (i + j + 1);
	//return sin((double) (3.0 * i + 7.0 * j + 1.0));
};

int main(int argc, char ** argv)
{
	int mpi_world_size_sq;
	int mpi_size;
	int myid;
	int myid_I;
	int myid_J;

	int mtx_size;

	MPI_Init(&argc, &argv);
	MPI_Comm_size(world, &mpi_world_size_sq);
	MPI_Comm_rank(world, &myid);

	mpi_size = sqrt(mpi_world_size_sq);
	mtx_size = matrix_size / mpi_size;
	if (mpi_size * mpi_size != mpi_world_size_sq || mtx_size * mpi_size != matrix_size)
	{
		if (myid == 0)
		{
			printf("The number of processes is not a full square or matrix size is not divisible by process count, exiting...");
		}; 

		MPI_Finalize();
		return -2;
	};

	myid_I = myid % mpi_size;
	myid_J = myid / mpi_size;

	char cN = 'N';
	double done = 1.0;
	double dzero = 0.0;
	double dmone = -1.0;
	int ione = 1;
	int izero = 0;

	double * mtx = new double[mtx_size * mtx_size];

	//filling my matrix parts
	int istart;
	int jstart;

	//matrix A
	istart = myid_I * mtx_size;
	jstart = myid_J * mtx_size;

	for(int i = 0; i < mtx_size; i++)
	{
		for(int j = 0; j < mtx_size; j++)
		{
			mtx[i + j * mtx_size] = MatrixFormula(istart + i, jstart + j);
		};
	};
	int main_I;
	int main_J;
	double err_stop = 1;
	double max_elem_old = 1.0;
	int count;
	if (myid == 0) {
		count = 1;
	}
	while (err_stop > 1.0e-5) {
		// searching max element
		double max_elem = 0.0;
		int max_i = istart;
		int max_j = jstart;
		for (int i = 0; i < mtx_size; i++) {
			for (int j = 0; j < mtx_size; j++) {
				if (fabs(mtx[i + j * mtx_size]) >= fabs(max_elem)) {
					max_elem = mtx[i + j * mtx_size];
					max_i = istart + i;
					max_j = jstart + j;
				}
			}
		}
		int *buf_i = new int[mpi_world_size_sq];
		int *buf_j = new int[mpi_world_size_sq];
		double *buf_val = new double[mpi_world_size_sq];

		MPI_Allgather(&max_i, 1, MPI_INT, buf_i, 1, MPI_INT, world);
		MPI_Allgather(&max_j, 1, MPI_INT, buf_j, 1, MPI_INT, world);
		MPI_Allgather(&max_elem, 1, MPI_DOUBLE, buf_val, 1, MPI_DOUBLE, world);
		for (int i = 0; i < mpi_world_size_sq; i++) {
			if (fabs(buf_val[i]) >= fabs(max_elem)) {
				max_elem = buf_val[i];
				max_i = buf_i[i];
				max_j = buf_j[i];
			}
		}
		err_stop = fabs(max_elem) / fabs(max_elem_old);
		if (myid == 0) {
			printf("%d %d %f \n", max_i, max_j, max_elem);
			//err_stop = fabs(max_elem) / fabs(max_elem_old);
			if (count > 0) max_elem_old = max_elem;
			count--;
		}

		double *row = new double [mtx_size];
		double *column = new double [mtx_size];
		double *row_get = new double [mtx_size];
		double *column_get = new double [mtx_size];

		// part 2
		// 6 блок
		int proc_i = max_i / mtx_size;
		int proc_j = max_j / mtx_size;
		MPI_Request req_row, req_col;
		MPI_Status info;
		MPI_Irecv(column_get, mtx_size, MPI_DOUBLE, myid_I + proc_j * mpi_size, 0, world, &req_col);
		if (myid_J == proc_j) {
			// отправляем по столбцам
			// <->
			for (int i = 0; i < mtx_size; i++) {
				column[i] = mtx[i + (max_j % mtx_size) * mtx_size]; 
			}
			for (int j = 0; j < mpi_size; j++) {
				MPI_Send(column, mtx_size, MPI_DOUBLE, myid_I + j * mpi_size, 0, world);
			}
		}
		MPI_Wait(&req_col, &info);

		MPI_Irecv(row_get, mtx_size, MPI_DOUBLE, myid_J * mpi_size + proc_i, 0, world, &req_row);

		if (myid_I == proc_i) {
			// отправляем по строчкам
			// ^
			// |
			// v
			for (int j = 0; j < mtx_size; j++) {
				row[j] = mtx[max_i % mtx_size + j * mtx_size];
			}	
			for (int i = 0; i < mpi_size; i++) {
				MPI_Send(row, mtx_size, MPI_DOUBLE, i + myid_J * mpi_size, 0, world);
			}
		}
		MPI_Wait(&req_row, &info);

		for (int i = 0; i < mtx_size; i++) {
			for (int j = 0; j < mtx_size; j++) {
				mtx[i + j * mtx_size] -= row_get[j] * column_get[i] / max_elem;
			}
		}

		delete[] row;
		delete[] row_get;
		delete[] column;
		delete[] column_get;
		delete[] buf_i;
		delete[] buf_j;
		delete[] buf_val;
	}
	delete[] mtx;

	MPI_Finalize();
};
