# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "Fortran"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_Fortran
  "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/src/lapack/extension/dlamch.f" "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/lapack/extension/CMakeFiles/lapack_ext-3.2.dir/dlamch.f.o"
  )
set(CMAKE_Fortran_COMPILER_ID "GNU")
set(CMAKE_Fortran_SUBMODULE_SEP "@")
set(CMAKE_Fortran_SUBMODULE_EXT ".smod")

# The include file search paths:
set(CMAKE_Fortran_TARGET_INCLUDE_PATH
  "../src/aniFEM"
  "../src/aniMBA"
  "../src/aniC2F"
  "../src/aniINB"
  "../src/aniLU/UMFPACK/Include"
  "../src/aniLU/AMD/Include"
  "../src/aniLU/UFconfig"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
