# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/src/aniC2F/ani2D.c" "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/aniC2F/CMakeFiles/c2f2D-3.1.dir/ani2D.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../src/aniFEM"
  "../src/aniMBA"
  "../src/aniC2F"
  "../src/aniINB"
  "../src/aniLU/UMFPACK/Include"
  "../src/aniLU/AMD/Include"
  "../src/aniLU/UFconfig"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
