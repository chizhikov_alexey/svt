# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/src/aniAFT/aft2d.c" "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/aniAFT/CMakeFiles/aft2D-3.1.dir/aft2d.c.o"
  "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/src/aniAFT/memory2.c" "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/aniAFT/CMakeFiles/aft2D-3.1.dir/memory2.c.o"
  "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/src/aniAFT/refine2.c" "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/aniAFT/CMakeFiles/aft2D-3.1.dir/refine2.c.o"
  "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/src/aniAFT/region2.c" "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/aniAFT/CMakeFiles/aft2D-3.1.dir/region2.c.o"
  "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/src/aniAFT/struct2.c" "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/aniAFT/CMakeFiles/aft2D-3.1.dir/struct2.c.o"
  "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/src/aniAFT/tree2.c" "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/aniAFT/CMakeFiles/aft2D-3.1.dir/tree2.c.o"
  "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/src/aniAFT/tria2.c" "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/aniAFT/CMakeFiles/aft2D-3.1.dir/tria2.c.o"
  "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/src/aniAFT/user2.c" "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/aniAFT/CMakeFiles/aft2D-3.1.dir/user2.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../src/aniFEM"
  "../src/aniMBA"
  "../src/aniC2F"
  "../src/aniINB"
  "../src/aniLU/UMFPACK/Include"
  "../src/aniLU/AMD/Include"
  "../src/aniLU/UFconfig"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
