file(REMOVE_RECURSE
  "CMakeFiles/fem2D-3.1.dir/DG.f.o"
  "CMakeFiles/fem2D-3.1.dir/__/aniIO/io.f.o"
  "CMakeFiles/fem2D-3.1.dir/algebra.f.o"
  "CMakeFiles/fem2D-3.1.dir/assemble.f.o"
  "CMakeFiles/fem2D-3.1.dir/bc.f.o"
  "CMakeFiles/fem2D-3.1.dir/dof.f.o"
  "CMakeFiles/fem2D-3.1.dir/error.f.o"
  "CMakeFiles/fem2D-3.1.dir/fem2Dedge.f.o"
  "CMakeFiles/fem2D-3.1.dir/fem2Derr.f.o"
  "CMakeFiles/fem2D-3.1.dir/fem2Dsub.f.o"
  "CMakeFiles/fem2D-3.1.dir/fem2Dtri.f.o"
  "CMakeFiles/fem2D-3.1.dir/forlibfem.f.o"
  "CMakeFiles/fem2D-3.1.dir/isys.f.o"
  "CMakeFiles/fem2D-3.1.dir/opCURL.f.o"
  "CMakeFiles/fem2D-3.1.dir/opDIV.f.o"
  "CMakeFiles/fem2D-3.1.dir/opDUDN.f.o"
  "CMakeFiles/fem2D-3.1.dir/opDUDX.f.o"
  "CMakeFiles/fem2D-3.1.dir/opGRAD.f.o"
  "CMakeFiles/fem2D-3.1.dir/opIDEN.f.o"
  "CMakeFiles/fem2D-3.1.dir/opTRACE.f.o"
  "CMakeFiles/fem2D-3.1.dir/template.f.o"
  "CMakeFiles/fem2D-3.1.dir/utils.f.o"
  "libfem2D-3.1.a"
  "libfem2D-3.1.pdb"
)

# Per-language clean rules from dependency scanning.
foreach(lang Fortran)
  include(CMakeFiles/fem2D-3.1.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
