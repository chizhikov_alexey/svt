# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "Fortran"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_Fortran
  "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/src/Tutorials/PackageAFT/main_boundary_square.f" "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/Tutorials/PackageAFT/CMakeFiles/aniAFT_boundary_square.exe.dir/main_boundary_square.f.o"
  )
set(CMAKE_Fortran_COMPILER_ID "GNU")
set(CMAKE_Fortran_SUBMODULE_SEP "@")
set(CMAKE_Fortran_SUBMODULE_EXT ".smod")

# The include file search paths:
set(CMAKE_Fortran_TARGET_INCLUDE_PATH
  "../src/aniFEM"
  "../src/aniMBA"
  "../src/aniC2F"
  "../src/aniINB"
  "../src/aniLU/UMFPACK/Include"
  "../src/aniLU/AMD/Include"
  "../src/aniLU/UFconfig"
  "../include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/aniAFT/CMakeFiles/aft2D-3.1.dir/DependInfo.cmake"
  "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/aniVIEW/CMakeFiles/view2D-3.1.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
