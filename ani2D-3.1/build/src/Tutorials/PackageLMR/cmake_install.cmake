# Install script for directory: /home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/src/Tutorials/PackageLMR

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "0")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/../bin/aniLMR_nodal.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/../bin/aniLMR_nodal.exe")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/../bin/aniLMR_nodal.exe"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/../bin/aniLMR_nodal.exe")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/../bin" TYPE EXECUTABLE FILES "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/Tutorials/PackageLMR/aniLMR_nodal.exe")
  if(EXISTS "$ENV{DESTDIR}/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/../bin/aniLMR_nodal.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/../bin/aniLMR_nodal.exe")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/../bin/aniLMR_nodal.exe"
         OLD_RPATH "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/../lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/../bin/aniLMR_nodal.exe")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/../bin/aniLMR_edgeest.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/../bin/aniLMR_edgeest.exe")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/../bin/aniLMR_edgeest.exe"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/../bin/aniLMR_edgeest.exe")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/../bin" TYPE EXECUTABLE FILES "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/Tutorials/PackageLMR/aniLMR_edgeest.exe")
  if(EXISTS "$ENV{DESTDIR}/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/../bin/aniLMR_edgeest.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/../bin/aniLMR_edgeest.exe")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/../bin/aniLMR_edgeest.exe"
         OLD_RPATH "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/../lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/../bin/aniLMR_edgeest.exe")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/../bin/aniLMR_gradient.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/../bin/aniLMR_gradient.exe")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/../bin/aniLMR_gradient.exe"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/../bin/aniLMR_gradient.exe")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/../bin" TYPE EXECUTABLE FILES "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/Tutorials/PackageLMR/aniLMR_gradient.exe")
  if(EXISTS "$ENV{DESTDIR}/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/../bin/aniLMR_gradient.exe" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/../bin/aniLMR_gradient.exe")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/../bin/aniLMR_gradient.exe"
         OLD_RPATH "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/../lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/../bin/aniLMR_gradient.exe")
    endif()
  endif()
endif()

