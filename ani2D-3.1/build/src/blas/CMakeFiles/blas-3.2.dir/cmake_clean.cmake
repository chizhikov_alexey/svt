file(REMOVE_RECURSE
  "CMakeFiles/blas-3.2.dir/daxpy.f.o"
  "CMakeFiles/blas-3.2.dir/dcopy.f.o"
  "CMakeFiles/blas-3.2.dir/ddot.f.o"
  "CMakeFiles/blas-3.2.dir/dgemm.f.o"
  "CMakeFiles/blas-3.2.dir/dgemv.f.o"
  "CMakeFiles/blas-3.2.dir/dger.f.o"
  "CMakeFiles/blas-3.2.dir/dnrm2.f.o"
  "CMakeFiles/blas-3.2.dir/drot.f.o"
  "CMakeFiles/blas-3.2.dir/drotg.f.o"
  "CMakeFiles/blas-3.2.dir/dscal.f.o"
  "CMakeFiles/blas-3.2.dir/dswap.f.o"
  "CMakeFiles/blas-3.2.dir/dsymv.f.o"
  "CMakeFiles/blas-3.2.dir/dsyr.f.o"
  "CMakeFiles/blas-3.2.dir/dsyr2.f.o"
  "CMakeFiles/blas-3.2.dir/dsyr2k.f.o"
  "CMakeFiles/blas-3.2.dir/dsyrk.f.o"
  "CMakeFiles/blas-3.2.dir/dtrmm.f.o"
  "CMakeFiles/blas-3.2.dir/dtrmv.f.o"
  "CMakeFiles/blas-3.2.dir/dtrsm.f.o"
  "CMakeFiles/blas-3.2.dir/dtrsv.f.o"
  "CMakeFiles/blas-3.2.dir/idamax.f.o"
  "CMakeFiles/blas-3.2.dir/lsame.f.o"
  "CMakeFiles/blas-3.2.dir/xerbla.f.o"
  "libblas-3.2.a"
  "libblas-3.2.pdb"
)

# Per-language clean rules from dependency scanning.
foreach(lang Fortran)
  include(CMakeFiles/blas-3.2.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
