# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "Fortran"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_Fortran
  "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/src/aniIO/io.f" "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/aniPRJ/CMakeFiles/prj2D-3.1.dir/__/aniIO/io.f.o"
  "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/src/aniPRJ/metamesh.f" "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/aniPRJ/CMakeFiles/prj2D-3.1.dir/metamesh.f.o"
  "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/src/aniPRJ/rhs.f" "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/aniPRJ/CMakeFiles/prj2D-3.1.dir/rhs.f.o"
  "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/src/aniPRJ/tri2tri.f" "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/aniPRJ/CMakeFiles/prj2D-3.1.dir/tri2tri.f.o"
  )
set(CMAKE_Fortran_COMPILER_ID "GNU")
set(CMAKE_Fortran_SUBMODULE_SEP "@")
set(CMAKE_Fortran_SUBMODULE_EXT ".smod")

# The include file search paths:
set(CMAKE_Fortran_TARGET_INCLUDE_PATH
  "../src/aniFEM"
  "../src/aniMBA"
  "../src/aniC2F"
  "../src/aniINB"
  "../src/aniLU/UMFPACK/Include"
  "../src/aniLU/AMD/Include"
  "../src/aniLU/UFconfig"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
