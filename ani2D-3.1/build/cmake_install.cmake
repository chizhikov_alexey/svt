# Install script for directory: /home/a.chizhikov/Programs/SVT/svt/ani2D-3.1

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "0")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/aniAFT/cmake_install.cmake")
  include("/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/aniFEM/cmake_install.cmake")
  include("/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/aniILU/cmake_install.cmake")
  include("/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/aniLU/cmake_install.cmake")
  include("/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/aniINB/cmake_install.cmake")
  include("/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/aniLMR/cmake_install.cmake")
  include("/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/aniMBA/cmake_install.cmake")
  include("/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/aniPRJ/cmake_install.cmake")
  include("/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/aniRCB/cmake_install.cmake")
  include("/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/aniC2F/cmake_install.cmake")
  include("/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/aniVIEW/cmake_install.cmake")
  include("/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/blas/cmake_install.cmake")
  include("/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/lapack/cmake_install.cmake")
  include("/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/lapack/extension/cmake_install.cmake")
  include("/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/Tutorials/PackageAFT/cmake_install.cmake")
  include("/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/Tutorials/PackageC2F/cmake_install.cmake")
  include("/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/Tutorials/PackageFEM/cmake_install.cmake")
  include("/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/Tutorials/PackageILU/cmake_install.cmake")
  include("/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/Tutorials/PackageLU/cmake_install.cmake")
  include("/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/Tutorials/PackageINB/cmake_install.cmake")
  include("/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/Tutorials/PackageLMR/cmake_install.cmake")
  include("/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/Tutorials/PackageMBA/cmake_install.cmake")
  include("/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/Tutorials/PackagePRJ/cmake_install.cmake")
  include("/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/Tutorials/PackageRCB/cmake_install.cmake")
  include("/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/Tutorials/PackageVIEW/cmake_install.cmake")
  include("/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/python/cmake_install.cmake")
  include("/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/Tutorials/MultiPackage/Interpolation/cmake_install.cmake")
  include("/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/Tutorials/MultiPackage/ConvectionDiffusion/cmake_install.cmake")
  include("/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/Tutorials/MultiPackage/HybridMixedFem/cmake_install.cmake")
  include("/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/Tutorials/MultiPackage/IsotropicBVP/cmake_install.cmake")
  include("/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/Tutorials/MultiPackage/NonisotropicBVP1/cmake_install.cmake")
  include("/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/Tutorials/MultiPackage/NonisotropicBVP2/cmake_install.cmake")
  include("/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/Tutorials/MultiPackage/PosterioriEstimates/cmake_install.cmake")
  include("/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/Tutorials/MultiPackage/Stokes/cmake_install.cmake")
  include("/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/Tutorials/MultiPackage/StokesNavier/cmake_install.cmake")
  include("/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/Tutorials/MultiPackage/UnsteadyConDif/cmake_install.cmake")
  include("/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/src/Tutorials/MultiPackage/DiscontinuousGalerkin/cmake_install.cmake")

endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/home/a.chizhikov/Programs/SVT/svt/ani2D-3.1/build/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
